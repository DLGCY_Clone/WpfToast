﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media.Animation;
using WpfToast.Controls;

namespace WpfToast
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            SetAlignment();
            TBPath.Text = AppDomain.CurrentDomain.BaseDirectory;

            DataContext = new MainWidowViewModel();
        }

        private void Toast_Click(object sender, RoutedEventArgs e)
        {
            ShowAllToast(new Thickness(2));
        }

        /// <summary>
        /// 显示各方位的气泡弹框
        /// </summary>
        private void ShowAllToast(Thickness margin)
        {
            ToastIcons icon = ToastIcons.Information;
            Toast.Show("容器左上角", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.OwnerTopLeft });
            Toast.Show("容器上中间", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.OwnerTopCenter });
            Toast.Show("容器右上角", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.OwnerTopRight });
            Toast.Show("容器左中间", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.OwnerLeft });
            Toast.Show("容器正中间", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.OwnerCenter, Closed = closed, Click = click });
            Toast.Show("容器右中间", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.OwnerRight });
            Toast.Show("容器左下角", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.OwnerBottomLeft });
            Toast.Show("容器下中间", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.OwnerBottomCenter });
            Toast.Show("容器右下角", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.OwnerBottomRight });
            Toast.Show("屏幕左上角", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.ScreenTopLeft });
            Toast.Show("屏幕上中间", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.ScreenTopCenter });
            Toast.Show("屏幕右上角", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.ScreenTopRight });
            Toast.Show("屏幕左中间", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.ScreenLeft });
            Toast.Show("屏幕正中间", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.ScreenCenter });
            Toast.Show("屏幕右中间", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.ScreenRight });
            Toast.Show("屏幕左下角", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.ScreenBottomLeft });
            Toast.Show("屏幕下中间", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.ScreenBottomCenter });
            Toast.Show("屏幕右下角", new ToastOptions { Icon = icon, ToastMargin = margin, Time = 5000, Location = ToastLocation.ScreenBottomRight });
        }

        private void click(object sender, EventArgs e)
        {
            Toast toast = sender as Toast;
            if (toast == null)
            {
                return;
            }
            toast.Close();
            Console.WriteLine($"===>Click!");
        }

        public void closed(object sender, EventArgs e)
        {
            Console.WriteLine($"===>Closed!");
        }

        private void Toast2_OnClick(object sender, RoutedEventArgs e)
        {
            Toast.Show("屏幕上中间 AAAAABBBBBCCCCCDDDDDEEEEEFFFFFGGGGGHHHHH", 
                new ToastOptions { Icon = ToastIcons.Information, Location = ToastLocation.ScreenTopCenter, Time = 5000 });

            Toast.Show("容器上中间 AAAAABBBBBCCCCCDDDDDEEEEEFFFFFGGGGGHHHHH",
                new ToastOptions { Location = ToastLocation.OwnerTopCenter, Time = 5000 });

            Toast.Show("容器左中间(ToastWidth=150) AAAAABBBBBCCCCCDDDDDEEEEEFFFFFGGGGGHHHHH",
                new ToastOptions { Location = ToastLocation.OwnerLeft, Time = 5000, ToastWidth = 150, ToastHeight = 100});

            Toast.Show("容器右中间(TextWidth=150) AAAAABBBBBCCCCCDDDDDEEEEEFFFFFGGGGGHHHHH",
                new ToastOptions { Location = ToastLocation.OwnerRight, Time = 5000, TextWidth = 150 });
        }

        private void Toast3_OnClick(object sender, RoutedEventArgs e)
        {
            ShowAllToast(new Thickness(5, 10, 5, 10));
        }

        private void BtnShowPlaceTip_OnClick(object sender, RoutedEventArgs e)
        {
            Resources["IsShowPlaceTest"] = true;
        }

        private void BtnHidePlaceTip_OnClick(object sender, RoutedEventArgs e)
        {
            Resources["IsShowPlaceTest"] = false;
        }

        private void Popup_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Popup popup = sender as Popup;
            if (popup == null)
            {
                return;
            }

            popup.IsOpen = false;
        }

        private void TBPath_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            TextBox tb = sender as TextBox;
            Toast.Show($"{tb?.Text}", new ToastOptions { Location = ToastLocation.OwnerTopCenter, Time = 5000 });
        }

        /// <summary>
        /// 转换系统的左撇子显示模式为右撇子显示模式
        /// </summary>
        public static void SetAlignment()
        {
            //获取系统是以Left-handed（true）还是Right-handed（false）
            var ifLeft = SystemParameters.MenuDropAlignment;

            if (ifLeft)
            {
                Console.WriteLine($"系统为左撇子，转换为右撇子。");

                // change to false
                var t = typeof(SystemParameters);
                var field = t.GetField("_menuDropAlignment", BindingFlags.NonPublic | BindingFlags.Static);
                field.SetValue(null, false);

                ifLeft = SystemParameters.MenuDropAlignment;
            }
        }
    }

    public class MainWidowViewModel : MyBindableBase
    {

    }
}
