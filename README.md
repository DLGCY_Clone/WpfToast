# WpfToast

## 介绍

[分享一个 WPF 气泡弹框](http://dlgcy.com/Share-A-WPF-Bubble-Popup-Like-Toast)

[WPF 中自制类似安卓消息提示框 Toast 控件](https://blog.csdn.net/weixin_44448313/article/details/107469089)

[原始源码](https://download.csdn.net/download/weixin_44448313/12646474)

## 效果展示

![Snipaste_2022-04-19_17-00-59.png](/images/Snipaste_2022-04-19_17-00-59.png)

## 说明

后续如果有更新，应该是在 [WPFTemplateLib](https://gitee.com/dlgcy/WPFTemplateLib) / [DLGCY_WPFPractice](https://gitee.com/dlgcy/DLGCY_WPFPractice) 中进行了。
